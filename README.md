PICO-8 Piet IDE
===============

This is a completely impractical project to build an IDE for my favorite esoteric language,
[Piet](http://www.dangermouse.net/esoteric/piet.html), in [PICO-8](https://www.lexaloffle.com/pico-8.php),
the fantasy console better-suited for making actual games (which I did eventually
[also do](https://www.lexaloffle.com/bbs/?tid=31185), for some definition of "game").

As of this writing, it's a fairly complete but not terribly-well-tested implementation of
the Piet language, written from scratch. The `in` command is not yet implemented, mostly
because I haven't figured out how best to do that sensibly in PICO-8 and haven't bothered
with the debug mode yet, but the rest of the language is there.

I've built it up and tested it with increasingly complex programs - it now runs my own
Piet implementation of a [Tablua Recta](https://codegolf.stackexchange.com/questions/86986/print-a-tabula-recta/89354#89354)
from a Code Golf challenge, which is a bit of a torture test and does all kinds of odd
things. In fact, while developing the Tabula Recta I found and fixed a bug in the excellent
[npiet](https://www.bertnase.de/npiet/) that had stood for two years. So, it should handle
most things, at least.

The main limitation is image size - I have ideas on how to be cleverer about this, with
compression and the like, but for now the maximum image size is limited by the uncompressed
gfx+map memory. Each pixel takes just over half a byte because Piet has 20 colors (argh!),
and currently that's a full byte, so with 12k of memory between them, you get a maximum canvas
of 64x192 pixels. Theoretically those could be any dimensions, but I haven't tested images wider
than the gfx memory (128px/64 bytes) and stuff would probably break.

Getting images into Piet is accomplished with the IMPORT command to load the image as a spritesheet.
The `ditherize.py` utility will take an image with Piet's native palette and translate it into a
PNG that will import as a sprite sheet, including duplicating each pixel into a pair to allow
specifying more than 16 colors, and adding a metadata border for the image loader.

Convert the image with `ditherize.py original.png` - this will generate `original.pico8.png`,
which can then be loaded into the spritesheet with `IMPORT original.pico8.png` within PICO-8.
The program will recognize that sprite data has been imported and convert it to the native
working format on startup. The native format includes a sentinel pixel that is invalid for
imported images to differentiate.

Note that imported images are limited by the size of the sprite sheet. Since there are two PICO-8
pixels per byte and the implementation requires a full byte per Piet pixel, the available size is
64x128.
